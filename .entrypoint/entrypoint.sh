#!/bin/bash

chown -R root:root ${HOME}/.ssh

file=../themes/twentyseventeen/style.css
if [ ! -e "$file" ]; then
  composer require --dev wpackagist-theme/twentyseventeen
fi

composer install

/usr/sbin/apachectl -D FOREGROUND
