# Docker/Wordpress experiment
This repo goes along with the (very experimental) uconn/wordpress Docker image. The goal is to create a project environment where people only need docker as a dependency.

## Docker
The Docker image comes ready to go with:

- composer
- a full WP install based on the `johnpbloch/wordpress` stable version

The Docker image has an entrypoint to begin the apache server. However, since no themes are built into the image, the entrypoint script in the repo will install the twentyseventeen theme if it doesn't exist. If you choose to mount the entrypoint script in the repo, make sure to change permissions before trying to run it.
```
chmod +x ./.entrypoint/entrpoint.sh
```

## Dependencies
To add plugins, use
```
docker-compose exec web composer require --dev {plugin-name}
```

To add plugins/themes from our satis repo you'll need the bitbucket private ssh key. The `.ssh` directory should be mounted from the host (as an absolute path) to the container - `${HOME}/.ssh:/root/.ssh`. 

## Known Issues
- The initial download of the image is quite large due to having a whole WP install already
- customizing the WP install for things like multisite, wp-uploads, etc... will require creating/mounting directories into the host. not sure if this is really an issue, but it's worth mentioning.