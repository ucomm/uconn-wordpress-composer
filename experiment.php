<?php

/**
 * Plugin Name: Experiment
 * Description: A test of a self-contained docker install
 * Author: UComm
 * Text Domain: experiment
 * Version: 0.0.1
 */

function hello_world() {
  echo 'Hello World';
}

add_action('init', 'hello_world');